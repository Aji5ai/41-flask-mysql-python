# Flask - Interagir avec une base de données MySQL

Apprends à utiliser MySQL avec Flask.

## Ressources

- [Flask-MySQLdb documentation](https://pypi.org/project/Flask-MySQLdb/)
- [Installer et utiliser Postman](https://www.youtube.com/watch?v=vMdhZvmRPe0)

## Contexte du projet

Dans ce cours, nous allons explorer l'utilisation de `flask-mysqldb` pour connecter une application Flask à une base de données MySQL. Ce module offre une interface simple pour interagir avec MySQL, permettant de créer des routes dynamiques basées sur les données.

### Mise en place d'une base de données

Le code suivant établit une base de données pour une bibliothèque :

```sql
CREATE DATABASE flask_library;

USE flask_library;
```

La base contiendra une table avec les champs suivants :

```sql
CREATE TABLE book (
	book_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	title VARCHAR(100) NOT NULL,
	author VARCHAR(100) NOT NULL
);
```

Un utilisateur sera créé pour accéder à cette base et avoir les droits sur la table :

```sql
CREATE USER 'libadm1475'@'localhost' IDENTIFIED BY 'TjE!CcAAKYxfoP@fY@fBYX8va7Fi8jjv';
GRANT ALL PRIVILEGES ON flask_library.* TO 'libadm1475'@'localhost';
FLUSH PRIVILEGES;
```

Enfin, des livres d'exemples seront ajoutés à la base de données :

```sql
INSERT INTO book (title, author) VALUES ('Les Misérables', 'Victor Hugo');
INSERT INTO book (title, author) VALUES ('Le Comte de Monte-Cristo', 'Alexandre Dumas');
INSERT INTO book (title, author) VALUES ('Orgueil et Préjugés', 'Jane Austen');
INSERT INTO book (title, author) VALUES ('Crime et Châtiment', 'Fyodor Dostoïevski');
INSERT INTO book (title, author) VALUES ('Les Aventures de Sherlock Holmes', 'Arthur Conan Doyle');
INSERT INTO book (title, author) VALUES ('Moby Dick', 'Herman Melville');
INSERT INTO book (title, author) VALUES ('Le Tour du Monde en Quatre-Vingts Jours', 'Jules Verne');
INSERT INTO book (title, author) VALUES ("L'Iliade", 'Homère');
INSERT INTO book (title, author) VALUES ("L'Odyssée", 'Homère');
INSERT INTO book (title, author) VALUES ('Frankenstein', 'Mary Shelley');
```

### Initialisation d'un projet Flask

Avant d'installer le nécessaire pour accéder à la base de données, initialise le projet de façon à ce qu'il ait Flask de fonctionnel.

Pense à activer l'environnement virtuel !

### Installation de flask-mysqldb

Installe `flask-mysqldb` dans ton projet Flask :

```bash
pip install flask-mysqldb
```

> Sous Linux, tu devras d'abord installer les dépendances suivantes :
```bash
sudo apt-get install python3-dev default-libmysqlclient-dev build-essential
```

### Connexion à la base de données

Après avoir installé le module, établis une connexion à ta base de données MySQL avec les informations de connexion appropriées.

Le code suivant va permettre de te connecter à la base de données.

Crée un fichier `app.py` et définis une route pour interagir avec ta base de données MySQL.

```python
from flask import Flask, jsonify, request
from flask_mysqldb import MySQL

app = Flask(__name__)
# Configuration de la connexion à la base de données
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'ton_utilisateur' # Remplacez par 'libadm1475'
app.config['MYSQL_PASSWORD'] = 'ton_mot_de_passe' # Remplacez par 'TjE!CcAAKYxfoP@fY@fBYX8va7Fi8jjv'
app.config['MYSQL_DB'] = 'ta_base_de_donnees' # Remplacez par 'flask_library'

# Crée un objet MySQL pour interagir avec la base de données
mysql = MySQL(app)

# Les futures routes seront ajoutées ici
```

Lance ton serveur et vérifie qu'aucune erreur ne s'affiche.

### Création d'une route pour afficher la liste des livres

Une fois que ton serveur fonctionne et est correctement connecté à la base de données, tu ajouteras une route d'API qui servira à afficher la liste des livres :

```python
@app.route('/books', methods=['GET'])
def get_book_list():
    # Crée un curseur pour exécuter des requêtes SQL
    cursor = mysql.connection.cursor()
    # Sélectionne tous les livres de la table book
    cursor.execute('SELECT * FROM book')
    # Récupère tous les résultats de la requête
    books = cursor.fetchall()
    # Convertit les résultats en JSON et les renvoie
    return jsonify(books)
```

Lance le serveur et essaie d'accéder à la route [http://localhost:5000/books](http://localhost:5000/books). Si tout est bien configuré, la liste des livres devrait apparaître !

### Requêtes préparées

Maintenant, tu vas ajouter une route permettant d'afficher un livre grâce à son identifiant.

Voici deux méthodes : une dangereuse, qui permettrait les injections SQL :

```python
# Mauvaise pratique - susceptible aux injections SQL
# par exemple : http://localhost:5000/books/'; DROP TABLE book; --
@app.route('/books/<id>', methods=['GET'])
def get_book(id):
    cursor = mysql.connection.cursor()
    # Utilise la concaténation de chaînes, risque d'injection SQL
    cursor.execute(f"SELECT * FROM book WHERE book_id = '{id}'")
    book = cursor.fetchone()
    return jsonify(book)
```

Pour éviter ce problème de sécurité, utilises les requêtes préparées :

```python
# Bonne pratique - requête préparée
@app.route('/books/<int:id>', methods=['GET'])
def get_book(id):
    cursor = mysql.connection.cursor()
    # Utilise une requête préparée, plus sécurisée
    cursor.execute('SELECT * FROM book WHERE book_id = %s', (id,))
    book = cursor.fetchone()
    if book:
        return jsonify(book)
    else:
        return 'Livre non trouvé', 404
```

### Tester ses routes d'API

Jusqu'à présent, tu as utilisé ton navigateur web pour tester tes routes d'API : tant qu'il s'agissait de faire des requêtes en GET, ça allait. Cependant, les requêtes suivantes vont utiliser des méthodes POST, PUT et DELETE, que ton navigateur web ne peut pas gérer !

Dans un cas comme celui-ci, il faut utiliser un outil spécifique. Il en existe plusieurs, je te conseille **Postman**.

Prends le temps de regarder cette vidéo jusqu'au bout pour voir comment **Postman** fonctionne : [Installer et utiliser Postman](https://www.youtube.com/watch?v=vMdhZvmRPe0).

Ensuite, entraîne-toi à appeler les deux routes précédemment créées.

### CRUD

Tu as précédemment vu les deux routes **READ (GET)** pour sélectionner l'ensemble des livres, ou un seul livre grâce à son identifiant.

Voici des exemples couvrant le reste du **CRUD** :

- **Create (POST)**
```python
# création d'un livre
@app.route('/books', methods=['POST'])
def add_book():
    # Récupère les données envoyées en JSON
    data = request.get_json()
    # Extrait le titre du JSON
    title = data['title']
    # Extrait l'auteur du JSON
    author = data['author']
    cursor = mysql.connection.cursor()
    # Insère les données dans la base de données
    cursor.execute('INSERT INTO book (title, author) VALUES (%s, %s)', (title, author))
    # Applique les modifications
    mysql.connection.commit()
    return 'Livre ajouté', 201
```

- **Update (PUT)**
```python
# mise à jour d'un livre
@app.route('/books/<int:id>', methods=['PUT'])
def update_book(id):
    data = request.get_json()
    title = data['title']
    author = data['author']
    cursor = mysql.connection.cursor()
    # Met à jour les informations du livre
    cursor.execute('UPDATE book SET title = %s, author = %s WHERE book_id = %s', (title, author, id))
    mysql.connection.commit()
    return 'Livre mis à jour', 200
```

- **Delete (DELETE)**
```python
# suppression d'un livre
@app.route('/books/<int:id>', methods=['DELETE'])
def delete_book(id):
    cursor = mysql.connection.cursor()
    # Supprime le livre de la base de données
    cursor.execute('DELETE FROM book WHERE book_id = %s', (id,))
    mysql.connection.commit()
    return 'Livre supprimé', 200
```

Utilises **Postman** pour tester toutes les routes de ton API.

## Modalités pédagogiques

**Pré-requis :**

Crée la base de données `flask_garage`.

Crée un utilisateur `garadm7841` avec le mot de passe `SP7c3$@uwL84jmSEoP3` ayant tous les droits sur la base `flask_garage`.

Dans la base `flask_garage`, crée la table `car` avec les colonnes :
- `car_id` : clé primaire numérique, auto-incrémentée
- `brand` : non null, chaîne de taille maximum 255 caractères
- `model` : non null, chaîne de taille maximum 255 caractères

**Ensuite :**

- Crée un projet Flask utilisant `flask-mysqldb`.
- Configure une connexion pour te connecter à la base de données `flask_garage` avec l'utilisateur `garadm7841` et le mot de passe `SP7c3$@uwL84jmSEoP3`.
- Développe les routes du CRUD de la table `car`.
- Ajoute une requête qui permet de trouver les voitures d'une marque (*brand*) par son nom.
- Teste tes routes avec Postman.
- Documente ton code.
- Partage ton projet sur GitLab.

## Livrables

Un lien vers GitLab

## Critères de performance

- Coder de façon sécurisée les accès aux données relationnelles en consultation, création, mise à jour et suppression
- Inclure dans les composants d’accès l’authentification et la gestion de la sécurité du SGDB
- Connaissance d’un langage de requête de type SQL
- Connaissance des règles de sécurisation des composants d’accès aux données
- Le code source est documenté
- Utiliser les normes de codage du langage
- La documentation technique de l’environnement de travail est comprise
- Utiliser un outil de gestion de versions