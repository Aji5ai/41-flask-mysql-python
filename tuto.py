# Pour lancer ce fichier : 
# (potentiellement faire python -m venv venv avant le reste si la suite toute seule ne marche pas)
# source venv/Scripts/activate
# Puis :  flask run --debug (risque de ne lancer que app.js, dans ce cas voir juste après)
# Pour lancer un fichier en particulier : flask --app tuto.py run --debug 
# ou encore flask --app app.py run --debug (si pas déjà lancé avec flask run --debug)


from flask import Flask, jsonify, request
from flask_mysqldb import MySQL

app = Flask(__name__)
# Configuration de la connexion à la base de données
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'libadm1475'
app.config['MYSQL_PASSWORD'] = 'TjE!CcAAKYxfoP@fY@fBYX8va7Fi8jjv'
app.config['MYSQL_DB'] = 'flask_library'

# Crée un objet MySQL pour interagir avec la base de données
mysql = MySQL(app)

# Les futures routes seront ajoutées ici

# Affiche la liste de livres su l'url /books
@app.route('/books', methods=['GET'])
def get_book_list():
    # Crée un curseur pour exécuter des requêtes SQL
    cursor = mysql.connection.cursor()
    # Sélectionne tous les livres de la table book
    cursor.execute('SELECT * FROM book')
    # Récupère tous les résultats de la requête
    books = cursor.fetchall()
    # Convertit les résultats en JSON et les renvoie
    return jsonify(books)

# Route permettant d'afficher une livre grâce à son ID : 
# Bonne pratique - requête préparée
@app.route('/books/<int:id>', methods=['GET'])
def get_book(id):
    cursor = mysql.connection.cursor()
    # Utilise une requête préparée, plus sécurisée
    cursor.execute('SELECT * FROM book WHERE book_id = %s', (id,))
    book = cursor.fetchone()
    if book:
        return jsonify(book)
    else:
        return 'Livre non trouvé', 404

# Mauvaise pratique - susceptible aux injections SQL
# par exemple : http://localhost:5000/books/'; DROP TABLE book; --
# @app.route('/books/<id>', methods=['GET'])
# def get_book(id):
#     cursor = mysql.connection.cursor()
#     # Utilise la concaténation de chaînes, risque d'injection SQL
#     cursor.execute(f"SELECT * FROM book WHERE book_id = '{id}'")
#     book = cursor.fetchone()
#     return jsonify(book)


##########    IMPORTANT    ##########
# Jusqu'à présent, tu as utilisé ton navigateur web pour tester tes routes d'API : tant qu'il s'agissait de faire des requêtes en GET, ça allait. Cependant, les requêtes suivantes vont utiliser des méthodes POST, PUT et DELETE, que ton navigateur web ne peut pas gérer !
# Dans un cas comme celui-ci, il faut utiliser un outil spécifique. Il en existe plusieurs, je te conseille Postman.
    
# création d'un livre
    # faire sur postman une requete POST puis dans Body insérer raw en JSON avec : 
    # {
    # "title": "H2G2",
    # "author": "Douglas Adams"
    # }
@app.route('/books', methods=['POST'])
def add_book():
    # Récupère les données envoyées en JSON
    data = request.get_json()
    # Extrait le titre du JSON
    title = data['title']
    # Extrait l'auteur du JSON
    author = data['author']
    cursor = mysql.connection.cursor()
    # Insère les données dans la base de données
    cursor.execute('INSERT INTO book (title, author) VALUES (%s, %s)', (title, author))
    # applique les modifications
    mysql.connection.commit()
    return 'Livre ajouté', 201

# mise à jour d'un livre
# Faire avec PUT et non PATCH sur postman car route Patch non exxistante dans ce code
# Mettre PUT, préciser à la fin de l'url l'id du livre à modifier, et en body, raaw, JSON, bien remettre toutes les informations du livre dont celle à modifier. Un patch permet de modifier qu'une partie et donc de ne réécrire qu'elle, mais le PUT demande de tout réécrire même ce qui ne sera pas modifié.
# ex avec http://127.0.0.1:5000/books/11
@app.route('/books/<int:id>', methods=['PUT'])
def update_book(id):
    data = request.get_json()
    title = data['title']
    author = data['author']
    cursor = mysql.connection.cursor()
    # Met à jour les informations du livre
    cursor.execute('UPDATE book SET title = %s, author = %s WHERE book_id = %s', (title, author, id))
    mysql.connection.commit()
    return 'Livre mis à jour', 200

# suppression d'un livre
# ex avec http://127.0.0.1:5000/books/11
# Pas besoin de préciser qqc dans le body, juste mettre l'id du livre à supprimer en fin d'url
@app.route('/books/<int:id>', methods=['DELETE'])
def delete_book(id):
    cursor = mysql.connection.cursor()
    # Supprime le livre de la base de données
    cursor.execute('DELETE FROM book WHERE book_id = %s', (id,))
    mysql.connection.commit()
    return 'Livre supprimé', 200
