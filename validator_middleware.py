from flask import jsonify, request
from functools import wraps # utilisé pour mettre à jour la fonction décorée pour qu'elle ressemble davantage à la fonction originale (avant la décoration). Cela est particulièrement utile lors de la création de décorateurs, car cela permet de conserver les métadonnées telles que le nom de la fonction, la documentation et d'autres attributs importants.model

# Ce middleware permet de vérifier que la brand et le model ont bien été renseignés avant de faire la requete de POST ou PUT
def validate_car_data(next): #next c'est la fonction à décorer, la fonction sur laquelle le décorateur sera appelé
    @wraps(next) # la fonction wrapper conserve les métadonnées de la fonction next qui est la fonction à laquelle validate_car_data sera appliqué.
    def wrapper(*args, **kwargs): #args c'est les arguments de next, et kwargs les arguments qui ont une valeur par défaut de renseignée (si y'a des kwargs dans next)
        data = request.json
        brand = data.get('brand')
        model = data.get('model')

        # Vérifier que brand et model sont présents dans la requête
        if not brand or not model:
            return jsonify({'message': 'La marque et le modèle de voiture sont obligatoires.'}), 400
        else:
            # passe au middleware suivant
            return next(*args, **kwargs)

    return wrapper

# On fait pareil mais pour vérifier les garages :
# On va vérifier l'email donc on importe re
import re # module Python intégré qui fournit des fonctions pour travailler avec des expressions régulières (regex)
# et on défini l'expression regex à vérifier :
regex = re.compile(r'([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+') # compiles the regex pattern into a regex object
"""
([A-Za-z0-9]+[.-_])* : Correspond à un groupe qui permet de spécifier que le nom d'utilisateur peut commencer par une séquence de lettres majuscules et minuscules ainsi que des chiffres, suivie par un point, un trait d'union ou un underscore. Cette séquence peut se répéter zéro ou plusieurs fois.

[A-Za-z0-9]+ : Correspond à un groupe qui spécifie que le nom d'utilisateur doit se terminer par une séquence d'une ou plusieurs lettres majuscules et minuscules ainsi que des chiffres.

@ : Correspond simplement au caractère @, qui est requis dans une adresse e-mail.

[A-Za-z0-9-]+ : Correspond à un groupe qui spécifie que le nom de domaine peut contenir des lettres majuscules et minuscules, des chiffres et des tirets.

(\.[A-Z|a-z]{2,})+ : Correspond à un groupe qui spécifie que le nom de domaine doit être suivi d'au moins un point suivi par au moins deux lettres majuscules ou minuscules.
"""

def validate_garage_data(next): #next c'est la fonction à décorer, la fonction sur laquelle le décorateur sera appelé
    @wraps(next) # la fonction wrapper conserve les métadonnées de la fonction next qui est la fonction à laquelle validate_garage_data sera appliqué.
    def wrapper(*args, **kwargs): #args c'est les arguments de next, et kwargs les arguments qui ont une valeur par défaut de renseignée (si y'a des kwargs dans next)
        data = request.json
        name = data.get('name')
        email = data.get('email')

        # Vérifier que name et model sont présents dans la requête
        if not name or not email:
            return jsonify({'message': 'Le nom et l\'email du garage sont obligatoires.'}), 400
        elif not re.fullmatch(regex, email):
            return jsonify({'message': 'Le format de l\'email entré n\'est pas valide'}), 400
        else:
            # passe au middleware suivant
            return next(*args, **kwargs)

    return wrapper
