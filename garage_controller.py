from flask import Blueprint, request, jsonify
# jsonify = utilisé pour générer des réponses JSON à partir de données Python
# request = permet d'accéder aux données de la requête HTTP entrante

# Tu auras besoin de ta connexion à la base de données pour faire les requêtes SQL
from app import mysql
# Pour importer le middleware qui permet de vérifier que les données ont bien été renseignées dans la requête
from validator_middleware import validate_garage_data

garage_controller = Blueprint('garage', __name__)

# Affiche la liste de garages sur l'url /
@garage_controller.route('/', methods=['GET'])
def get_garage_list():
    # Crée un curseur pour exécuter des requêtes SQL
    cursor = mysql.connection.cursor()
    # Sélectionne tous les garages de la table garage
    cursor.execute('SELECT * FROM garage')
    # Récupère tous les résultats de la requête
    garage = cursor.fetchall()
    # Convertit les résultats en JSON et les renvoie
    return jsonify(garage) # Mais ne rend pas un tableau d'objets mais un tableau de tableaux. Donc dans le prochain brief on va corriger ça avec un dictionnaire !

# Route permettant d'afficher une garage grâce à son ID : 
# Bonne pratique - requête préparée
@garage_controller.route('/<int:id>', methods=['GET'])
def get_garage(id):
    cursor = mysql.connection.cursor()
    # Utilise une requête préparée, plus sécurisée
    cursor.execute('SELECT * FROM garage WHERE garage_id = %s', (id,))
    garage = cursor.fetchone()
    if garage:
        return jsonify(garage)
    else:
        return 'garage non trouvé', 404

##########    IMPORTANT    ##########
# Jusqu'à présent, tu as utilisé ton navigateur web pour tester tes routes d'API : tant qu'il s'agissait de faire des requêtes en GET, ça allait. Cependant, les requêtes suivantes vont utiliser des méthodes POST, PUT et DELETE, que ton navigateur web ne peut pas gérer !
# Dans un cas comme celui-ci, il faut utiliser un outil spécifique. Il en existe plusieurs, je te conseille Postman.
    
# création d'un garage
    # faire sur postman une requete POST puis dans Body insérer raw en JSON avec : 
    # {
    #     "name": "Garage5",
    #     "email": "garage5@email.com"
    # }
@garage_controller.route('/', methods=['POST'])
# ajoute le middleware avec le décorateur @validate_garage_data
@validate_garage_data
def add_garage():
    # Récupère les données envoyées en JSON
    data = request.get_json()
    # Extrait le titre du JSON
    name = data['name']
    # Extrait l'auteur du JSON
    email = data['email']
    cursor = mysql.connection.cursor()
    # Insère les données dans la base de données
    cursor.execute('INSERT INTO garage (name, email) VALUES (%s, %s)', (name, email))
    # Applique les modifications
    mysql.connection.commit()
    return 'garage ajouté', 201

# mise à jour d'un garage
# Faire avec PUT et non PATCH sur postman garage route Patch non existante dans ce code
# Mettre PUT, préciser à la fin de l'url l'id de la garage à modifier, et en body, raaw, JSON, bien remettre toutes les informations de la garage dont celle à modifier. Un patch permet de modifier qu'une partie et donc de ne réécrire qu'elle, mais le PUT demande de tout réécrire même ce qui ne sera pas modifié.
# ex avec http://127.0.0.1:5000/5
@garage_controller.route('/<int:id>', methods=['PUT'])
# ajoute le middleware avec le décorateur @validate_garage_data
@validate_garage_data
def update_garage(id):
    data = request.get_json()
    name = data['name']
    email = data['email']
    cursor = mysql.connection.cursor()
    # Met à jour les informations du garage
    cursor.execute('UPDATE garage SET name = %s, email = %s WHERE garage_id = %s', (name, email, id))
    mysql.connection.commit()
    return 'garage mis à jour', 200

# suppression d'un garage
# ex avec http://127.0.0.1:5000/5
# Pas besoin de préciser qqc dans le body, juste mettre l'id du garage à supprimer en fin d'url
@garage_controller.route('/<int:id>', methods=['DELETE'])
def delete_garage(id):
    cursor = mysql.connection.cursor()
    # Supprime le garage de la base de données
    cursor.execute('DELETE FROM garage WHERE garage_id = %s', (id,))
    mysql.connection.commit()
    return 'garage supprimé', 200
