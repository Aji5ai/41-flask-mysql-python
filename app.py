# Pour lancer ce fichier : 
# (potentiellement faire python -m venv venv avant le reste si la suite toute seule ne marche pas)
# source venv/Scripts/activate
# Puis :  flask run --debug
# Pour lancer un fichier en particulier comme tuto (qui est dans le même dossier) : flask --app tuto.py run --debug

from flask import Flask, request, jsonify
# flask = classe principale de Flask utilisée pour créer l'application web

from flask_bcrypt import Bcrypt

from flask_mysqldb import MySQL #  importe la classe MySQL de l'extension Flask-MySQLdb, qui simplifie l'utilisation de MySQL avec Flask.
from flask_cors import CORS # importe le module CORS (Cross-Origin Resource Sharing (partage des ressources entre origines différentes))
from dotenv import load_dotenv # Cela charge les variables d'environnement à partir du fichier .env dans l'application Flask.
load_dotenv()

# On peut mettre le nom de variable qu'on veut, par convention c'est app
app = Flask(__name__) #  Crée une instance de la classe Flask et l'assigne à la variable app. __name__ est une variable spéciale en Python qui représente le nom du module actuel. Cela est généralement utilisé pour déterminer le chemin racine du serveur.

bcrypt = Bcrypt(app)

CORS(app, resources={r"/*": {"origins": "http://localhost:8080"}}) # Configuration pour autoriser une origine particulière 

# Configuration de la connexion à la base de données disponible dans config.py et importée ici
app.config.from_object('config')

# Crée un objet MySQL pour interagir avec la base de données
mysql = MySQL(app) # Cette ligne crée une instance de la classe MySQL en utilisant l'application Flask app comme argument. Cela configure Flask pour interagir avec la base de données MySQL en utilisant les paramètres fournis précédemment. La variable mysql est utilisée pour exécuter des requêtes MySQL dans le code ultérieur de l'application Flask.

# importe le middleware logger qui va logger chaque requête (collecter les informations de chaque appel à l'API) avant qu'elle n'atteigne le controller de cars. Cela permet de suivre et d'analyser les requêtes entrantes.
from logger_middleware import log_request
# initialise le middleware pour logger les requêtes
app.before_request(log_request)

# importe les blueprints une fois la base de données initialisée
from cars_controller import cars_controller
from garage_controller import garage_controller

app.register_blueprint(cars_controller, url_prefix='/cars') # Enregistre le Blueprint cars_controller dans l'application Flask principale (app). Cela signifie que les routes définies dans cars_controller seront accessibles avec le préfixe /cars. Par exemple, si cars_controller définit une route /list, elle sera accessible dans l'application en tant que /cars/list.
app.register_blueprint(garage_controller, url_prefix='/garages')

    # Ajout de requetes pour création de compte, login etc
# Pour que l'utilisateur puisse créer un compte (email et mdp):
@app.route('/register', methods=['POST']) 
# necessite le json suivant en body : 
# {
#   "email": "test@account.com",
#   "password": "tacostacos"
# }
def register():
    # Récupération des données JSON di body de la requête :
    data = request.get_json()
    email = data.get('email')
    password = data.get('password')

    # Vérification que email et password renseignés avant d'aller plus loin :
    if (email is None or password is None):
        return jsonify({'error': 'Please specify both email and password'}), 401
    else :
        # hacher le mot de passe
        hashed_password = bcrypt.generate_password_hash(
            password
        ).decode('utf-8') # bcrypt.generate_password_hash retourne des octets qu'il faut convertir en utf8 (chaine de caractères) (on pourrait stocker en octets dans la BDD cela dit)

        # enregistrer le compte en base de données : ajoute email et mdp haché
        cursor = mysql.connection.cursor()
        cursor.execute(
            'INSERT INTO user (email, password) VALUES (%s, %s)',
            (email, hashed_password)
        )
        mysql.connection.commit()
        user_id = cursor.lastrowid # renvoie l'ID de la dernière ligne insérée à l'aide du curseur actuel

        # on retourne le compte créé (mais sans le mot de passe !)
        user_inserted = {
            'user_id': user_id,
            'email': email
        }
        return jsonify(user_inserted), 201

# Pour que l'utilisateur puisse se connecter :
@app.route('/login', methods=['POST'])
# ex de JSON nécessaire dans la body de la requete :
# {
#   "email": "test@account.com",
#   "password": "tacostacos"
# }

def login():
    # Récupération des données JSON di body de la requête :
    data = request.get_json()
    email = data.get('email')
    password = data.get('password')

    # Vérification que email et password renseignés avant d'aller plus loin :
    if (email is None or password is None):
        return jsonify({'error': 'Please specify both email and password'}), 401
    else:
        # Vérifier qu'un compte existe bien avec l'email fourni.
        cursor = mysql.connection.cursor()
        cursor.execute(
            'SELECT * FROM user WHERE email = %s',
            (email,)
        )
        user = cursor.fetchone() # si aucun utilisateur n'a été trouvé avec l'email (car email incorrect par ex), la requête va retourner un user = none

        if (user is None): # si on ne retrouve pas d'utilisateur avec le mail fourni :
                return jsonify({'error': 'Invalid email'}), 401
        else: # si on a bien l'utilisateur :
            # Récupérer le mot de passe haché en base de données.
            hashed_password = user['password'] # marche si MYSQL_CURSORCLASS = 'DictCursor' dans config car sinon ça sera considéré comme un tupple et faudra plutôt utiliser les indices [0] par ex
            
            # Comparer avec bcrypt le mot de passe haché et le mot de passe fourni en clair.
            if (not bcrypt.check_password_hash(hashed_password, password)):
                return jsonify({'error': 'Invalid password'}), 401

            # Retourner le compte connecté (mais sans le mot de passe !).
            return jsonify({
                'id': user['user_id'],
                'email': user['email'],
            }), 200
