from flask import request
from datetime import datetime

def log_request():
    # Affiche la date, la méthode HTTP et l'URL de la requête
    print(f"Log: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: {request.method} {request.url}")
