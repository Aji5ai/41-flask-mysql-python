# Flask - Contrôleurs et Blueprint

Découvre la notion de contrôleurs et utilise Flask Blueprint dans ton application.

## Ressources

- [Vidéo - Qu’est-ce Qu’une APPLICATION MULTI-COUCHES En Développement](https://www.youtube.com/watch?v=L3A1KRJMZBA)
- [Flask Blueprint](https://flask.palletsprojects.com/en/3.0.x/blueprints/)

## Contexte du projet

Le contexte du projet implique la compréhension de la notion d'application multi-couche, où différentes couches, telles que le client, le serveur, et la base de données, interagissent de manière organisée et séparée.

Pour approfondir cette notion, consulte la [vidéo explicative suivante](https://www.youtube.com/watch?v=L3A1KRJMZBA).

Elle va donner des termes que tu ne maîtrises pas encore : pas de soucis, nous verrons tout cela par la suite !

### Modèle-Vue-Contrôleur (MVC)

Le modèle-vue-contrôleur (MVC) est un design pattern d'architecture qui divise une application en trois composants principaux :
- le modèle (data et logique métier)
- la vue (interface utilisateur)
- et le contrôleur (gestion des interactions utilisateur).

Cette approche permet de maintenir (corriger et mettre à jour) plus facilement le code de son application.

![Modèle MVC](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/Model-View-Controller_architectural_pattern-fr.svg/1920px-Model-View-Controller_architectural_pattern-fr.svg.png)

### Contrôleur

Un contrôleur est le composant du modèle MVC chargé de gérer les interactions utilisateur et d'orchestrer les actions à effectuer en réponse à ces interactions. Il agit comme une passerelle entre la vue et le modèle, traitant les requêtes et retournant les réponses appropriées.

Dans le cadre d'une API, le contrôleur va recevoir les requêtes HTTP, puis "router" (rediriger) la demande aux modèles (la couche liée à la base de données). Une fois la réponse des modèles récupérée, le contrôleur va la renvoyer en réponse, souvent au format JSON.

Dans le cadre de notre application `flask_library`, tout le code de notre application est actuellement contenu dans un seul fichier : cela rend complexe sa maintenabilité !

Améliorons cela avec la création de contrôleurs.

### Routage Flask avec Blueprint

Flask offre la possibilité de structurer vos routes en utilisant des `Blueprints`. Cela permet de modulariser le code en déplaçant la logique de routage dans des fichiers distincts. La [documentation sur Flask Blueprint](https://flask.palletsprojects.com/en/3.0.x/blueprints/) fournit des informations détaillées.

Dans le cadre du projet `flask_library`, nous allons déplacer le code des routes `/books` dans un Blueprint `books_controller.py`.

Voici un exemple concret :

1. **Crée le fichier `books_controller.py` :**

```python
# books_controller.py
from flask import Blueprint, request, jsonify
# Tu auras besoin de ta connexion à la base de données pour faire les requêtes SQL
from app import mysql

books_controller = Blueprint('books', __name__)

# Déplacer et adapter les routes vers /books
@books_controller.route('/', methods=['GET'])
def get_books():
    # TODO: Déplacer ici le code de la route GET /books

@books_controller.route('/<int:id>', methods=['GET'])
def get_book(id):
    # TODO: Déplacer ici le code de la route GET /books/:id

@books_controller.route('/', methods=['POST'])
def create_book():
    # TODO: Déplacer ici le code de la route POST /books

@books_controller.route('/<int:id>', methods=['PUT'])
def update_book(id):
    # TODO: Déplacer ici le code de la route PUT /books/:id

@books_controller.route('/<int:id>', methods=['DELETE'])
def delete_book(id):
    # TODO: Déplacer ici le code de la route DELETE /books/:id
```

2. **Modifie ton fichier `app.py` pour utiliser le Blueprint :**

```python
# app.py
from flask_cors import CORS
from flask import Flask
from flask_mysqldb import MySQL
from dotenv import load_dotenv

load_dotenv()

app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "http://localhost:8080"}})
app.config.from_object('config')

mysql = MySQL(app)

# importe les blueprints une fois la base de données initialisée
from books_controller import books_controller

app.register_blueprint(books_controller, url_prefix='/books')
```

### Création d'autres contrôleurs avec Blueprint

Dans le cadre d'une API, chaque Blueprint aura la responsabilité d'une ressource de l'application.

Prenons comme exemple une nouvelle ressource `publisher` :

```python
# publisher_controller.py
from flask import Blueprint, jsonify

publisher_controller = Blueprint('publishers', __name__)

@publisher_controller.route('/', methods=['GET'])
def get_publishers():
    # TODO: Logique pour récupérer tous les éditeurs depuis la base de données
```

```python
# app.py

# Configurations ici...

# Importe les Blueprints
from books_controller import books_controller
from publisher_controller import publisher_controller

# Utilise les Blueprints pour gérer les routes
app.register_blueprint(books_controller, url_prefix='/books')
app.register_blueprint(publisher_controller, url_prefix='/publishers')
```

## Modalités pédagogiques

- Reprends ton projet `flask_garage`.
- Déplace tes routes dans un nouveau contrôleur.
- Modifie le code du fichier `app.py` afin de charger ton Blueprint.
- Dans ta base de données, crée une nouvelle table `garage` avec les champs `garage_id` (INT PRIMARY KEY AUTO_INCREMENT), `name` (VARCHAR(255) NOT NULL), et `email` (VARCHAR(255) NOT NULL).
- Crée un Blueprint `garage_controller.py` avec un CRUD complet, puis appelle ton Blueprint dans le fichier `app.py`.
- Versionne ton projet sur GitLab.

## Livrables

Un lien vers GitLab.

## Critères de performance

- Le code source est documenté.
- La documentation technique de l’environnement de travail est comprise.
- Utiliser un outil de gestion de versions.