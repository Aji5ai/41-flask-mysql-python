# Configuration de la connexion à la base de données
# Les lignes suivantes définissent la configuration de la base de données MySQL pour l'application Flask :
import os # module standard de Python qui fournit une interface pour interagir avec le système d'exploitation, y compris la gestion des variables d'environnement. Aucune installation n'est nécessaire, car os est inclus avec l'installation standard de Python.

# les valeurs de configuration sont modifiées pour utiliser os.getenv pour récupérer les valeurs à partir des variables d'environnement définies dans le fichier .env. Cela permet de ne pas stocker les informations sensibles directement dans le code source.
MYSQL_HOST = os.getenv('MYSQL_HOST') # os.getenv est une fonction Python qui est utilisée pour récupérer la valeur d'une variable d'environnement
MYSQL_USER = os.getenv('MYSQL_USER')
MYSQL_PASSWORD = os.getenv('MYSQL_PASSWORD')
MYSQL_DB = os.getenv('MYSQL_DB')
MYSQL_CURSORCLASS = 'DictCursor'
