from flask import Blueprint, request, jsonify
# jsonify = utilisé pour générer des réponses JSON à partir de données Python
# request = permet d'accéder aux données de la requête HTTP entrante

# Tu auras besoin de ta connexion à la base de données pour faire les requêtes SQL
from app import mysql
# Pour importer le middleware qui permet de vérifier que les données ont bien été renseignées dans la requête
from validator_middleware import validate_car_data

cars_controller = Blueprint('cars', __name__)

# Affiche la liste de voitures sur l'url /
@cars_controller.route('/', methods=['GET'])
def get_car_list():
    # Crée un curseur pour exécuter des requêtes SQL
    cursor = mysql.connection.cursor()
    # Sélectionne tous les voitures de la table car
    cursor.execute('SELECT * FROM car')
    # Récupère tous les résultats de la requête
    cars = cursor.fetchall()
    # Convertit les résultats en JSON et les renvoie
    return jsonify(cars) # Mais ne rend pas un tableau d'objets mais un tableau de tableaux. Donc dans le prochain brief on va corriger ça avec un dictionnaire !

# Route permettant d'afficher une voiture grâce à son ID : 
# Bonne pratique - requête préparée
@cars_controller.route('/<int:id>', methods=['GET'])
def get_car(id):
    cursor = mysql.connection.cursor()
    # Utilise une requête préparée, plus sécurisée
    cursor.execute('SELECT * FROM car WHERE car_id = %s', (id,))
    car = cursor.fetchone()
    if car:
        return jsonify(car)
    else:
        return 'voiture non trouvée', 404

##########    IMPORTANT    ##########
# Jusqu'à présent, tu as utilisé ton navigateur web pour tester tes routes d'API : tant qu'il s'agissait de faire des requêtes en GET, ça allait. Cependant, les requêtes suivantes vont utiliser des méthodes POST, PUT et DELETE, que ton navigateur web ne peut pas gérer !
# Dans un cas comme celui-ci, il faut utiliser un outil spécifique. Il en existe plusieurs, je te conseille Postman.
    
# création d'un voiture
    # faire sur postman une requete POST puis dans Body insérer raw en JSON avec : 
    # {
    #     "brand": "Renault",
    #     "model": "Captur"
    # }
@cars_controller.route('/', methods=['POST'])
# ajoute le middleware avec le décorateur @validate_car_data
@validate_car_data
def add_car():
    # Récupère les données envoyées en JSON
    data = request.get_json()
    # Extrait le titre du JSON
    brand = data['brand']
    # Extrait l'auteur du JSON
    model = data['model']
    cursor = mysql.connection.cursor()
    # Insère les données dans la base de données
    cursor.execute('INSERT INTO car (brand, model) VALUES (%s, %s)', (brand, model))
    # Applique les modifications
    mysql.connection.commit()
    return 'voiture ajoutée', 201

# mise à jour d'un voiture
# Faire avec PUT et non PATCH sur postman car route Patch non existante dans ce code
# Mettre PUT, préciser à la fin de l'url l'id de la voiture à modifier, et en body, raaw, JSON, bien remettre toutes les informations de la voiture dont celle à modifier. Un patch permet de modifier qu'une partie et donc de ne réécrire qu'elle, mais le PUT demande de tout réécrire même ce qui ne sera pas modifié.
# ex avec http://127.0.0.1:5000/5
@cars_controller.route('/<int:id>', methods=['PUT'])
# ajoute le middleware avec le décorateur @validate_car_data
@validate_car_data
def update_car(id):
    data = request.get_json()
    brand = data['brand']
    model = data['model']
    cursor = mysql.connection.cursor()
    # Met à jour les informations du voiture
    cursor.execute('UPDATE car SET brand = %s, model = %s WHERE car_id = %s', (brand, model, id))
    mysql.connection.commit()
    return 'voiture mise à jour', 200

# suppression d'une voiture
# ex avec http://127.0.0.1:5000/5
# Pas besoin de préciser qqc dans le body, juste mettre l'id du voiture à supprimer en fin d'url
@cars_controller.route('/<int:id>', methods=['DELETE'])
def delete_car(id):
    cursor = mysql.connection.cursor()
    # Supprime le voiture de la base de données
    cursor.execute('DELETE FROM car WHERE car_id = %s', (id,))
    mysql.connection.commit()
    return 'voiture supprimée', 200
