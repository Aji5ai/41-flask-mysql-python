# Flask - Utilisation des variables d'environnement

Apprends à utiliser des variables d'environnement avec Flask pour une meilleure gestion de la configuration.

## Ressources

- [Python-dotenv](https://pypi.org/project/python-dotenv/)

## Contexte du projet

### Déplacement du code de connexion à la base de données

Voici le code actuel de notre application Flask :

```python
from flask import Flask, jsonify, request
from flask_mysqldb import MySQL

app = Flask(__name__)
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'libadm1475'
app.config['MYSQL_PASSWORD'] = 'TjE!CcAAKYxfoP@fY@fBYX8va7Fi8jjv'
app.config['MYSQL_DB'] = 'flask_library'

mysql = MySQL(app)
```

Pour améliorer la lisibilité, déplace la configuration de la base de données dans un fichier spécifique.

Crée un fichier `config.py` dans ton projet. Place-y la configuration de la base de données :

```python
MYSQL_HOST = 'localhost'
MYSQL_USER = 'libadm1475'
MYSQL_PASSWORD = 'TjE!CcAAKYxfoP@fY@fBYX8va7Fi8jjv'
MYSQL_DB = 'flask_library'
```

Dans `app.py`, importe ton fichier de configuration :

```python
from flask import Flask
from flask_mysqldb import MySQL
import config

app = Flask(__name__)
app.config.from_object('config')

mysql = MySQL(app)
```


Teste ton application : elle devrait fonctionner aussi bien qu'avant !

### Utilisation des variables d'environnement

Les variables d'environnement sont utilisées pour gérer les configurations externes à ton application, telles que les identifiants de base de données, sans les stocker directement dans le code source.

Cela améliore la sécurité et la flexibilité, en particulier pour les informations sensibles comme les mots de passe.

#### Étape 1 : Installer python-dotenv

Installe `python-dotenv` dans ton projet :

```bash
pip install python-dotenv
```

#### Étape 2 : Créer un fichier `.env`

Crée un fichier `.env` à la racine de ton projet et définis tes variables d'environnement :

```env
MYSQL_HOST=localhost
MYSQL_USER=libadm1475
MYSQL_PASSWORD=TjE!CcAAKYxfoP@fY@fBYX8va7Fi8jjv
MYSQL_DB=flask_library
```

#### Étape 3 : Configurer python-dotenv dans ton application

Ajoute ceci au début de ton fichier `app.py` (avant le chargement du fichier config):

```python
from dotenv import load_dotenv
load_dotenv()
```

#### Étape 4 : Utiliser les Variables d'Environnement

Modifie `config.py` pour utiliser ces variables :

```python
import os

MYSQL_HOST = os.getenv('MYSQL_HOST')
MYSQL_USER = os.getenv('MYSQL_USER')
MYSQL_PASSWORD = os.getenv('MYSQL_PASSWORD')
MYSQL_DB = os.getenv('MYSQL_DB')
```

#### Étape 5 : Ignorer `.env` et Créer un `.env.sample`

Ajoute `.env` à ton `.gitignore` pour ne pas le versionner. Crée un fichier `.env.sample` avec les clés vides :

```env
MYSQL_HOST=
MYSQL_USER=
MYSQL_PASSWORD=
MYSQL_DB=
```

Le fichier `.env.sample` sert de modèle pour les développeurs récupérant le projet, indiquant ce que le fichier `.env` doit contenir.

## Modalités Pédagogiques

- Reprends ton projet `flask_garage`.
- Déplace la configuration MySQL dans un fichier `config.py`.
- Crée deux fichiers `.env` et `.env.sample`, avec leur contenu.
- Inscrit `.env` au `.gitignore` avant de l'ajouter et de le commiter.
- Versionne ton projet sur GitLab.

## Livrables

Un lien vers GitLab

## Critères de performance

- Inclure dans les composants d’accès l’authentification et la gestion de la sécurité du SGDB
- Le code source est documenté
- La documentation technique de l’environnement de travail est comprise
- Utiliser un outil de gestion de versions