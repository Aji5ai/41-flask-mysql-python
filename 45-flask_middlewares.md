# Flask - Middlewares

Explorez le concept de middlewares dans le contexte d'une application Flask.

## Ressources

- [Python - Décorateurs](https://zestedesavoir.com/tutoriels/954/notions-de-python-avancees/2-functions/3-decorators/)

## Contexte du projet

Un middleware est une action qui s'intercale entre d'autres actions. Par exemple, supposons que j'appelle une API pour créer un nouveau livre. Cependant, avant d'essayer de créer ce livre, je souhaite vérifier que le titre et l'auteur sont correctement renseignés : je peux créer un **middleware** pour cette tâche !

Dans le cadre d'une API, un middleware est une fonction qui a accès à l'objet de la requête (`request`), à l'objet de la réponse (`response`), et à la fonction middleware suivante dans le cycle de requête-réponse d'une application Flask.

Les middlewares sont utilisés pour effectuer des tâches intermédiaires, telles que :
- l'analyse des données de la requête (ex : les données fournies sont-elles correctes ?)
- la validation des utilisateurs (ex : l'utilisateur a-t-il le droit d'accéder aux informations ?)
- l'ajout d'en-têtes à la réponse (ex : dans le cas de l'authentification par JWT que tu verras plus tard)
- etc...

Dans Flask, les middlewares peuvent être intégrés en utilisant des **décorateurs** ou enregistrés avec la méthode `before_request`. Chaque middleware a la possibilité de terminer le cycle de la requête-réponse ou de passer la main au middleware suivant.

### Décorateur

En Python, un décorateur est une fonction qui permet de modifier le comportement d'une autre fonction : cela permet d'étendre le comportement d'une fonction sans la modifier directement.

Imaginons une fonction dont le rôle est d'envoyer un colis à une adresse :

```python
def envoyer_colis(numero_colis, destination):
    # Supposons une opération d'envoi de colis
    print(f"Colis {numero_colis} envoyé à {destination}.")
```

Appel de la fonction :

```python
envoyer_colis("ABC123", "Paris")
```

Maintenant, je voudrais ajouter l'envoi d'une notification lors de l'expédition du colis, sans modifier le contenu de ma fonction `envoyer_colis`.

Je peux le faire à l'aide d'un décorateur :

```python
# Décorateur pour envoyer une notification de suivi
def envoyer_notification(fonction_a_decorer):
    def decoration(*args, **kwargs):
        # Envoie une notification de suivi
        numero_colis = args[0]  # Supposons que le numéro de colis est le premier argument
        print(f"Notification de suivi envoyée pour le colis {numero_colis}: Colis en cours d'expédition.")

        # Appel de la fonction originale à décorer (ici `envoyer_colis`)
        return fonction_a_decorer(*args, **kwargs)

    # L'appel de la fonction `envoyer_notification` renvoie l'appel de la fonction `decoration`
    return decoration

# On ajoute le décorateur
@envoyer_notification
def envoyer_colis(numero_colis, destination):
    # Supposons une opération d'envoi de colis
    print(f"Colis {numero_colis} envoyé à {destination}.")
```

Dans le contexte des décorateurs en Python, `args` et `kwargs` sont des noms conventionnels utilisés pour représenter les arguments positionnels et les arguments de mot-clé, respectivement.
- **args** (abréviation de "arguments") est une liste qui contient les valeurs des arguments positionnels passés à la fonction.
- **kwargs** (abréviation de "keyword arguments") est un dictionnaire qui contient les valeurs des arguments de mot-clé passés à la fonction.

Lorsque `envoyer_colis` est appelée, si elle a été décorée avec `@envoyer_notification`, voici l'enchaînement :

1. La fonction `envoyer_notification()` est appelée.
2. La fonction `envoyer_notification()` retourne l'appel de sa fonction interne (ici `decoration`).
3. Les actions de `decoration()` sont effectuées.
4. En retour de `decoration()`, c'est l'appel de la fonction décorée `envoyer_colis()` qui est renvoyé, avec ses paramètres d'origine "ABC123" et "Paris", dont les noms et valeurs des paramètres sont passés par `*args` et les valeurs par `**kwargs`.

Utiliser un décorateur revient au même que si la fonction était appelée de cette manière :

```python
envoyer_notification(envoyer_colis("ABC123", "Paris"))
```

### Middleware global

Dans le cadre du projet `flask_library`, nous allons intégrer des middlewares pour effectuer certaines tâches avant que la requête n'atteigne le contrôleur.

Voici un exemple concret : la création d'un middleware qui va logger les requêtes (collecter les informations de chaque appel à l'API)

1. **Crée le fichier `logger_middleware.py` :**

```python
# logger_middleware.py
from flask import request
from datetime import datetime

def log_request():
    # Affiche la date, la méthode HTTP et l'URL de la requête
    print(f"Log: {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: {request.method} {request.url}")
```

2. **Modifie le fichier `app.py` pour utiliser le middleware :**

```python
# app.py
from flask_cors import CORS
from flask import Flask
from flask_mysqldb import MySQL
from dotenv import load_dotenv

load_dotenv()
# import de la bibiothèque cors

app = Flask(__name__)
# ajout d'une configuration
CORS(app, resources={r"/*": {"origins": "http://localhost:8080"}})
app.config.from_object('config')

mysql = MySQL(app)

# importe le middleware
from logger_middleware import log_request
# initialise le middleware pour logger les requêtes
app.before_request(log_request)

# importe les blueprints une fois la base de données initialisée
from books_controller import books_controller

app.register_blueprint(books_controller, url_prefix='/books')
```

Dans cet exemple, le middleware `log_request` est utilisé pour logger chaque requête avant qu'elle n'atteigne le contrôleur des livres. Cela permet de suivre et d'analyser les requêtes entrantes.

### Middleware spécifique à une route

Dans le cas précédent, tu as mis en place un middleware pour tous les appels vers ton API. Il est cependant possible d'associer un middleware sur certaines routes spécifiques.

Par exemple, imagines que tu veuilles valider les données des requêtes de création (POST) et modification (PUT) d'un livre. Il serait judicieux de vérifier que le titre et l'auteur ont bien été renseignés, et renvoyer une erreur dans le cas contraire :

1. **Crée le fichier `validator_middleware.py` pour le middleware de validation :**

```python
# validator_middleware.py
from flask import jsonify, request
from functools import wraps

def validate_book_data(next):
    @wraps(next)
    def wrapper(*args, **kwargs):
        data = request.json
        title = data.get('title')
        author = data.get('author')

        # Vérifier que le titre et l'auteur sont présents dans la requête
        if not title or not author:
            return jsonify({'message': 'Le titre et l\'auteur sont obligatoires.'}), 400
        else:
            # passe au middleware suivant
            return next(*args, **kwargs)

    return wrapper
```

2. **Modifie le fichier `books_controller.py` pour utiliser le middleware de validation :**

```python
# books_controller.py
from flask import Blueprint, request, jsonify
from validator_middleware import validate_book_data

books_controller = Blueprint('books', __name__)

@books_controller.route('/', methods=['POST'])
# ajoute le middleware avec le décorateur @validate_book_data
@validate_book_data
def create_book():
    data = request.json
    title = data.get('title')
    author = data.get('author')

    # Le middleware de validation garantit que les données sont correctes à ce stade
    # ...

@books_controller.route('/<int:book_id>', methods=['PUT'])
# ajoute le middleware avec le décorateur @validate_book_data
@validate_book_data
def update_book(book_id):
    data = request.json
    title = data.get('title')
    author = data.get('author')

    # Le middleware de validation garantit que les données sont correctes à ce stade
    # ...
```

Dans cet exemple, le middleware `validate_book_data` est utilisé comme premier middleware dans les routes POST et PUT pour garantir que les données du livre sont valides avant de procéder à l'insertion ou à la mise à jour dans la base de données. Le middleware renvoie une réponse avec le statut 400 (Bad Request) si les données ne sont pas valides.

### Combiner les middlewares

Imaginons que nous avons créé un middleware d'authentification (tu verras cela prochainement). Il est possible d'enchaîner les middlewares lors de la création d'un endpoint, de la façon suivante :

```python
@books_controller.route('/', methods=['POST'])
@auth_middleware
@validate_book_data
def create_book():
    # ...
```

Tu peux aussi appeler des middlewares lors de la configuration du blueprint (cependant ils seront appelés pour toutes les routes du contrôleur) :

```python
app.register_blueprint(books_controller, url_prefix='/books', decorators=[auth_middleware])
```

## Modalités pédagogiques

- Reprends ton projet `flask_garage`.
- Ajoute et utilise les middlewares `logger_middleware.py` et `validator_middleware.py`
- Dans le fichier `validator_middleware.py`, ajoute une fonction `validate_garage_data`, où tu vérifieras que les données de la table `garage` sont bien fournies et que l'email est au bon format (tu devrais utiliser une **regex**)
- Versionne ton projet sur GitLab.

## Livrables

Un lien vers GitLab

## Critères de performance

- Le code source est documenté
- La documentation technique de l’environnement de travail est comprise
- Utiliser un outil de gestion de versions