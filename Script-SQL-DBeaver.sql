CREATE DATABASE flask_garage;

USE flask_garage;

CREATE USER 'garadm7841' @'localhost' IDENTIFIED BY 'SP7c3$@uwL84jmSEoP3';

GRANT ALL PRIVILEGES ON flask_garage.* TO 'garadm7841' @'localhost';

FLUSH PRIVILEGES;

CREATE TABLE car (
    car_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    brand VARCHAR(255) NOT NULL,
    model VARCHAR(255) NOT NULL
);

INSERT INTO
    car (brand, model)
VALUES
    ('Honda', 'Civic 11');

INSERT INTO
    car (brand, model)
VALUES
    ('Isuzu', 'D-max');

INSERT INTO
    car (brand, model)
VALUES
    ('Maserati', 'Ghibli');

INSERT INTO
    car (brand, model)
VALUES
    ('Pontiac', 'Catalina');

/* Brief 44 */
USE flask_garage CREATE TABLE garage (
    garage_id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL
);

INSERT INTO
    garage (name, email)
VALUES
    ('Garage1', 'garage1@email.com'),
    ('Garage2', 'garage2@email.com'),
    ('Garage3', 'garage3@email.com'),
    ('Garage4', 'garage4@email.com');
    
/* Brief 47 hashage */
CREATE TABLE user (
	user_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	email VARCHAR(255) NOT NULL UNIQUE,
	password VARCHAR(255) NOT NULL
);
