# Flask - Authentification et mot de passe

Découvre la différence entre le chiffrement (encryption), le hachage (hashing) et le salage (salting) dans le processus d'authentification.

## Ressources

- [Qu'est-ce que l'authentification ?](https://www.cloudflare.com/fr-fr/learning/access-management/what-is-authentication/)
- [Comparaison entre Chiffrement, ‘Hashing’ et ‘Salting’ - Quelles sont les différences ?](https://www.pingidentity.com/fr/resources/blog/post/encryption-vs-hashing-vs-salting.html)
- [Flask-Bcrypt](https://flask-bcrypt.readthedocs.io/en/1.0.1/)

## Contexte du projet

### Authentification et autorisation

L'authentification est le processus par lequel un système vérifie l'identité d'un utilisateur. Elle est souvent réalisée par un nom d'utilisateur et un mot de passe, mais peut inclure d'autres méthodes comme les empreintes digitales ou la reconnaissance faciale.

Il ne faut pas confondre authentification et autorisation, qui est le processus qui détermine ce qu'un utilisateur est autorisé à faire après avoir été authentifié. Par exemple, un utilisateur authentifié peut avoir l'autorisation d'accéder à certaines routes d'API ou d'effectuer des actions spécifiques sur la base de données.

Je te conseille la lecture de la ressource suivante, pour en apprendre plus : [Qu'est-ce que l'authentification ?](https://www.cloudflare.com/fr-fr/learning/access-management/what-is-authentication/).

### Mot de passe

Un système d'authentification répandu est la connexion par identifiant (souvent un email) et un mot de passe. Bien sûr, pour vérifier que ces informations sont correctes, il faut les sauvegarder quelque part : dans une base de données.

Garder les mots de passe en clair dans une base de données présente un risque majeur de sécurité. En cas de fuite de données, les mots de passe seraient immédiatement accessibles aux acteurs malveillants, compromettant la sécurité des utilisateurs.

#### Chiffrement (encryption)

Le chiffrement (souvent désigné par encryptage, bien que ce soit un anglicisme) est la transformation d'informations en un format codé pour prévenir l'accès non autorisé.

Le chiffrement est réversible : il est possible de retrouver le mot de passe d'origine avec la bonne clé, contrairement au hachage.

#### Hachage (hashing)

Le hachage est le processus de conversion des données en une empreinte unique de longueur fixe. Cette empreinte est pratiquement impossible à inverser, ce qui signifie qu'on ne peut pas retrouver les données originales à partir du hash.

En clair, il est possible de savoir qu'un hash correspond à un mot de passe, mais il n'est pas possible de retrouver un mot de passe à partir de son hash.

#### Salage (salting)

Le salage consiste à ajouter des données aléatoires au mot de passe avant de le hacher. Cela rend chaque hash unique, même pour des mots de passe identiques, augmentant ainsi la sécurité contre les attaques de type dictionnaire ou par force brute.

Les définitions ci-dessus sont très résumées. Je te conseille de te plonger dans la ressource suivante pour élargir tes horizons : [Comparaison entre Chiffrement, ‘Hashing’ et ‘Salting’ - Quelles sont les différences ?](https://www.pingidentity.com/fr/resources/blog/post/encryption-vs-hashing-vs-salting.html).

### Authentification avec Flask et Bcrypt

Maintenant que tu as la théorie, voyons en pratique comment réaliser une authentification par mot de passe avec Flask.

### Base de données

Nous allons faire une version minimale de l'authentification dans ce brief. Seules les informations de l'email et du mot de passe seront renseignées dans le compte utilisateur :

```sql
CREATE TABLE user (
	user_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	email VARCHAR(255) NOT NULL UNIQUE,
	password VARCHAR(255) NOT NULL
);
```

### Module Bcrypt

Pour suivre ce tutoriel, tu peux reprendre un projet existant, ou mieux : initialise un nouveau projet Python avec les modules `flask`, `python-dotenv`, `flask-cors`, `flask-mysqldb`.

Ensuite, configure l'accès à une base de données qui contiendra la table `user` donnée dans la section précédente.

Afin de hacher et de comparer les mots de passe, tu auras besoin d'installer et d'importer le module [Flask-Bcrypt](https://flask-bcrypt.readthedocs.io/en/1.0.1/).

```bash
# installe le module dans ton projet
pip install flask-bcrypt
```

```python
# importe le module en haut de ton fichier principal (ex: app.py)
from flask import Flask
from flask_bcrypt import Bcrypt

app = Flask(__name__)
bcrypt = Bcrypt(app)
```

### Un route `/register` de création de compte

Partons du principe que ton site frontend a un formulaire de connexion, qui envoie dans le corps de la requête un `JSON` contenant les informations de l'email et du mot de passe :

```json
{
  "email": "test@account.com",
  "password": "tacostacos"
}
```

Le mot de passe est donné "en clair" par le formulaire de connexion (ce qui n'est pas problématique car, à terme, la requête sera protégée par `HTTPS`).

La première étape va consister à récupérer les informations dans une route `/register` :

```python
@app.route('/register', methods=['POST'])
def register():
    data = request.get_json()
    email = data.get('email')
    password = data.get('password')
    if (email is None or password is None):
        return jsonify({'error': 'Please specify both email and password'}), 401

    # TODO hacher le mot de passe
```

Pour compléter la section `# TODO hacher le mot de passe`, tu vas utiliser le module bcrypt et sa méthode `bcrypt.generate_password_hash()` :

```python
# hacher le mot de passe
hashed_password = bcrypt.generate_password_hash(
    password
).decode('utf-8')

# TODO enregistrer le compte en base de données
```

Maintenant que tu as la variable `hashed_password`, qui contient le mot de passe haché, tu vas pouvoir compléter la section `# TODO enregistrer le compte en base de données` :

```python
# enregistrer le compte en base de données
cursor = mysql.connection.cursor()
cursor.execute(
    'INSERT INTO user (email, password) VALUES (%s, %s)',
    (email, hashed_password)
)
mysql.connection.commit()
user_id = cursor.lastrowid

# on retourne le compte créé (mais sans le mot de passe !)
user_inserted = {
    'user_id': user_id,
    'email': email
}
return jsonify(user_inserted), 201
```

Tu testeras ta nouvelle route avec Postman et tu vérifieras que le mot de passe est bien haché en base de données. Essaye de créer plusieurs comptes avec le même mot de passe (mais pas le même `email`, car il est unique) :
- Qu'est-ce que tu remarques concernant le hachage de ce même mot de passe ? 
    -> je remarque que les deux mots de passes hachés commencent pareil, par "$2b$12$" alors que la suite est aléatoire.
- Sais-tu pourquoi ?
    -> nombre de générations de salt qu'il y a eu + systeme de hachage utilisé. Sera toujours le même tant que même encryptage et autant de tours de salages.

Ces questions te permettront de comprendre l'importance du hachage et du salage dans la sécurisation des mots de passe.

### Un route `/login` de connexion

Maintenant que la création de compte est réalisée, passons à la connexion. Tu recevras en corps de requête les données suivantes :

```json
{
  "email": "test@account.com",
  "password": "tacostacos"
}
```

Il va falloir comparer le mot de passe donné par l'utilisateur avec son mot de passe haché stocké dans la base de données. Pour cela, tu utiliseras la méthode `bcrypt.check_password_hash()`.

Voici le processus :

- Vérifier qu'un compte existe bien avec l'email fourni.
- Si c'est le cas, récupérer le mot de passe haché en base de données.
- Comparer avec bcrypt le mot de passe haché et le mot de passe fourni en clair.

```python
@app.route('/login', methods=['POST'])
def login():
    data = request.get_json()
    email = data.get('email')
    password = data.get('password')
    if (email is None or password is None):
        return jsonify({'error': 'Please specify both email and password'}), 401

    # Vérifier qu'un compte existe bien avec l'email fourni.
    cursor = mysql.connection.cursor()
    cursor.execute(
        'SELECT * FROM user WHERE email = %s',
        (data.get('email'),)
    )
    user = cursor.fetchone()

    if (user is None):
        return jsonify({'error': 'Invalid email'}), 401

    # Récupérer le mot de passe haché en base de données.
    hashed_password = user['password']
    # Comparer avec bcrypt le mot de passe haché et le mot de passe fourni en clair.
    if (not bcrypt.check_password_hash(hashed_password, password)):
        return jsonify({'error': 'Invalid password'}), 401

    # Retourner le compte connecté (mais sans le mot de passe !).
    return jsonify({
        'id': user['user_id'],
        'email': user['email'],
    }), 200
```

### Faire persister la connexion ?

En l'état actuel de ton code, tu ne peux pas faire persister la connexion, c'est-à-dire faire en sorte que le serveur se souvienne que l'utilisateur est connecté.

Pour cela, tu auras plusieurs méthodes à découvrir par la suite, dont :
- Les sessions.
- Les jetons JsonWebToken (JWT).

## Modalités pédagogiques

- Reprends un projet Flask existant ou crée et configure un nouveau projet
- Crée ou reprend une base de données, et ajoute une table `user` avec les champs email (VARCHAR(255) UNIQUE NOT NULL) et password (VARCHAR(255) NOT NULL).
- Installe et importe le module `flask-bcrypt`.
- Ajoute et teste la route `/register`
- Ajoute et teste la route `/login`

## Livrables

Un lien vers GitLab

## Critères de performance

- Le code source est documenté
- La documentation technique de l’environnement de travail est comprise
- Utiliser un outil de gestion de versions