# Flask - Gestion des CORS

Apprends à gérer les Cross-Origin Resource Sharing (CORS) dans une application Flask pour permettre des interactions sécurisées entre différents domaines.

## Ressources

- [Flask-CORS extension](https://flask-cors.readthedocs.io/en/latest/)

## Contexte du projet

Les CORS, ou Cross-Origin Resource Sharing (partage des ressources entre origines différentes), sont un mécanisme qui te permet d'autoriser l'accès aux ressources d'un serveur depuis un autre domaine que celui du serveur.

Le terme "cross-origin" fait référence à une situation où les requêtes d'accès aux ressources proviennent d'un domaine, d'un protocole ou d'un port différent de celui du serveur qui fournit ces ressources.

## Pourquoi les problèmes de CORS se posent-ils même en localhost ?

Même si ton frontend et ton backend sont tous deux exécutés sur `localhost`, ils utilisent probablement des ports différents. Par exemple, ton serveur Flask (backend) pourrait écouter sur le port 5000, tandis que ton application frontend (comme une application React ou Vue) pourrait s'exécuter sur le port 8080.

Dans ce cas, une requête de `localhost:8080` vers `localhost:5000` est considérée comme une requête cross-origin, car les ports sont différents. Les navigateurs appliquent la politique de même origine (Same-Origin Policy), qui par défaut bloque les requêtes cross-origin pour des raisons de sécurité.

### Installation et configuration du module CORS de Flask

#### Étape 1 : Installer le module CORS

Exécute la commande suivante dans ton projet pour installer le module CORS :

```bash
pip install flask-cors
```

#### Étape 2 : Configurer CORS dans ton application

1. **Importe le module CORS** : Ajoute `from flask_cors import CORS` dans ton fichier principal de Flask.

2. **Configuration spécifique** : Voici la configuration pour autoriser une origine particulière :

```python
CORS(app, resources={r"/*": {"origins": "http://localhost:8080"}})
```

3. **Exemple** :

```python
from flask import Flask, jsonify, request
from flask_mysqldb import MySQL
from dotenv import load_dotenv
load_dotenv()
# import de la bibiothèque cors
from flask_cors import CORS

app = Flask(__name__)
# ajout d'une configuration
CORS(app, resources={r"/*": {"origins": "http://localhost:8080"}})
```

### Étape 3 : Tester la configuration CORS

En testant ton application avec **Postman**, tu ne rencontreras jamais d'erreur liée aux **CORS**.

Il va donc te falloir tester avec une application **frontend**. Je te propose [cette application React](https://gitlab.com/bastienapp/react-cors-5000) : clone le dépôt et lance l'application avec la commande `npm run dev`.

Il te faudra un terminal avec l'application frontend d'exécutée, et un autre terminal avec ton serveur backend d'exécuté.

Si tout est bien configuré, l'url [http://localhost:8080](http://localhost:8080) devrait t'afficher la liste des voitures.

> Attention : cette application frontend est faite pour fonctionner avec le garage, pas la bibliothèque !

## Modalités pédagogiques

- Reprends ton projet `flask_garage`.
- Installe le module `flask-cors`.
- Configure les CORS afin d'autoriser l'accès à `http://localhost:8080`.
- Teste que l'application React affiche la liste des voitures.
- Versionne ton projet sur GitLab.

## Livrables

Un lien vers GitLab

## Critères de performance

- Le code source est documenté
- La documentation technique de l’environnement de travail est comprise
- Utiliser un outil de gestion de versions